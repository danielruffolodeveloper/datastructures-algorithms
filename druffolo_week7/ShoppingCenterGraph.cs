﻿using System;
using System.Collections.Generic;
using QuickGraph;

namespace DataStructures_Algorithms
{
	public class ShoppingCenterGraph
	{
		//TODO: Declare a bidirectional POIGraph where nodes are of type PointOfInterest and Edge of type Edge<PointOfInterest
		BidirectionalGraph<PointOfInterest, GraphEdge<PointOfInterest>> POIsGraph = new BidirectionalGraph<PointOfInterest, GraphEdge<PointOfInterest>>();


		public ShoppingCenterGraph()
		{
			//TODO: Add vertices to your graph, using POIGraph.AddVertex( give it here a POI )
			//shopping center constructor

			//point of interest 1
			//_______________________________________________________________________
			PointOfInterest Adidas = new PointOfInterest()
			{
				Name = "Adidas",
				Description = "a shop for sports gear, clothing and accessories",
				Location = "Ground Level"


			};
			POIsGraph.AddVertex(Adidas);
			//_______________________________________________________________________
			//point of interest 2

			PointOfInterest Nike = new PointOfInterest()
			{
				Name = "Nike",
				Description = "a shop for sports gear, clothing and accessories",
				Location = "second"


			};
			POIsGraph.AddVertex(Nike);
			//_______________________________________________________________________

			//point of interest 3

			PointOfInterest EBGames = new PointOfInterest()
			{
				Name = "EB Games",
				Description = "a shop for selling games",
				Location = "second"


			};
			POIsGraph.AddVertex(EBGames);

			//_______________________________________________________________________

			PointOfInterest JBHIFI = new PointOfInterest()
			{
				Name = "JBHIFI",
				Description = "a electronics and music shop",
				Location = "Frst"


			};
			POIsGraph.AddVertex(JBHIFI);

			//_______________________________________________________________________
			PointOfInterest Coles = new PointOfInterest()
			{
				Name = "Coles",
				Description = "a shop for selling food and groceries",
				Location = "Second"


			};
			POIsGraph.AddVertex(Coles);

			//_______________________________________________________________________
			PointOfInterest Woolworths = new PointOfInterest()
			{
				Name = "woolworths",
				Description = "a shop for selling food and groceries",
				Location = "Ground Floor"


			};
			POIsGraph.AddVertex(Woolworths);

			//_______________________________________________________________________
			//TODO: Add Edges to your graph, using POIGraph.AddEdge( give it a new edge that links between two verticies)
			//Make sure to set Source/Target/Description and Distance

			//_______________________________________________________________________

			POIsGraph.AddEdge(new GraphEdge<PointOfInterest>
			{
				Source = Adidas,
				Target = Nike,
				Distance = 20,
				Description = "Adidas to Nike"
			//_______________________________________________________________________	
			//_______________________________________________________________________	

			});
			POIsGraph.AddEdge(new GraphEdge<PointOfInterest>
			{
				Source = Nike,
				Target = Adidas,
				Distance = 50,
				Description = "Nike to Adidas"

			});
			//_______________________________________________________________________
			POIsGraph.AddEdge(new GraphEdge<PointOfInterest>
			{
				Source = EBGames,
				Target = Woolworths,
				Distance = 80,
				Description = "EB Games to Woolworths"

			});

			POIsGraph.AddEdge(new GraphEdge<PointOfInterest>
			{
				Source = JBHIFI,
				Target = Coles,
				Distance = 23,
				Description = "JBHIFI to Coles"

			});

			POIsGraph.AddEdge(new GraphEdge<PointOfInterest>
			{
				Source = Coles,
				Target = EBGames,
				Distance = 21,
				Description = "Coles to EB Games"

			});

			POIsGraph.AddEdge(new GraphEdge<PointOfInterest>
			{
				Source = Woolworths,
				Target = Adidas,
				Distance = 24,
				Description = "Woolworths to Adidas"

			});

			POIsGraph.AddEdge(new GraphEdge<PointOfInterest>
			{
				Source = Woolworths,
				Target = JBHIFI,
				Distance = 24,
				Description = "Woolworths to JBHIFI"

			});





		}



		public List<PointOfInterest> FindPossibleRoutesFrom(string POIName)
		{
			List<PointOfInterest> resultList = new List<PointOfInterest>();
			foreach (PointOfInterest poi in POIsGraph.Vertices)
			{
				if (poi.Name.Equals(POIName))
				{
					foreach (GraphEdge<PointOfInterest>edge in POIsGraph.Edges)
					{
						if (edge.Source.Equals(poi))
						{
							resultList.Add(edge.Target);
						}

					}

				}
			}
			return resultList;
		}

	}
}
	


