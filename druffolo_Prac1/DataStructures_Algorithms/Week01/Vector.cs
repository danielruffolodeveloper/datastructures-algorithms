﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures_Algorithms.Week01
{
    

    

    [Serializable]
    public class Vector<T>
    {
        T[] data;
        const int DEFAULT_CAPACITY = 10;
        private int capacity;
        private int count = 0;
        int index = 0;

        public Vector()
        {
            data = new T[DEFAULT_CAPACITY];
        }
        public Vector(int CAPACITY)
        {
            data = new T[CAPACITY];
        }

        public void ExtendArray_Size (int addCapacity)
        {
            T[] tempData = new T[data.Length + addCapacity];
            Array.Copy(data, 0, tempData, 0, count);
            data = tempData;
        }

          
        public void Add(T element)
        {
            //i decided the use of extend Array because it provides better understanding of what we are doing 
            //it is more steps however it is clearer to understand

            if (count >= data.Length)
                ExtendArray_Size(DEFAULT_CAPACITY);
            data[count++] = element;
        }

        public void Insert(T element, int index )
        {
           
             if(count == data.Length) ExtendArray_Size(DEFAULT_CAPACITY);

            for (int i = count; i > index; i--)
                data[i] = data[i - 1];
            data[index] = element;
            count++;

            

                    
        }
        
        public bool Remove(T element)
        {
            int index = IndexOf(element);
            if (index > -1)
                return RemoveAt(index);
            return false;
        }

        private bool RemoveAt(int index)
        {
            if (index > count) throw new IndexOutOfRangeException("index out of range");

            //move all array elements 1 place left to resize array
            //iterte through array
            for (int i = index; i < count; i++)
                data[i] = data[i + 1];
            //resize the count field
            count--;

            return true;
        }

       

        public void Clear()
        {
            //
            //we set the array to its original state with 10 index points of NULL
            data = new T[DEFAULT_CAPACITY];
        }
        public bool Contains(T element)
            //return true IF the element exists int he vector list
        {
            if(IndexOf(element) > -1)
            return true;
            else
            return false;
        }
      
        public override string ToString()
        {
            string returnString = "";
        
        
            for (int i = 0; i < count; i++)
            {
                returnString += Convert.ToString(data[i])+"\t";
            }

            if (returnString != null)
                return returnString;
            else
            return "No values in the array";
        }

        public int IndexOf(T element)
        {
          
            return Array.IndexOf(data, element);
           
        }
        public int Capacity
        {
            get
            {
                return Count;
            }
            set
            {
                if (value < count)
                    throw new ArgumentOutOfRangeException();
                ExtendArray_Size(value);
            }
        }
        public int Count
        {
            get { return count; }
        }

        public void Min()
        {
            // we are using LINQ FUNCTION to achieve the least amount of steps possible (EXTENSION TASK 3)
            //This acieves O(1) as we no longer need to sort through the array with a for loop, the function loads
            //data in the array into a temp memory file and runs the loop instantly , theirfore the only processing step is calling the function
            //compared to using a forloop, you can eliminate 0.6 seconds each aprox
            Console.WriteLine("Minimum number is: {0}", data.Min());
        }

        public void Max()
        {
            Console.WriteLine("Max number is: {0}", data.Max());
        }



        public T this[int index]
        {
            get
            {
                return data[index]; 
            }
            set
            {
                if (index >= count)
                    throw new IndexOutOfRangeException();
                data[index] = value;
            }
        }
    }
}
