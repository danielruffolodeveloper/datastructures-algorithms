﻿using System;
using DataStructures_Algorithms;

namespace Runner
{
	public class Runner06_Task02 : IRunner
	{
		public Runner06_Task02()
		{
		}

		public void Run(string[] args)
		{
			BinarySearchTree<int> bst = new BinarySearchTree<int>();

			bst.Add(5);
			bst.Add(10);
			bst.Add(4);
			bst.Add(5);
			bst.Add(12);




	
			Console.WriteLine(" **** PRE ORDER **** ");
			bst.Traverse(TraversalMode.PRE, Console.Out);


			Console.WriteLine(" **** IN ORDER **** ");
			bst.Traverse(TraversalMode.IN, Console.Out);


			Console.WriteLine(" **** POST ORDER **** ");
			bst.Traverse(TraversalMode.POST, Console.Out);


		}
	}
}
