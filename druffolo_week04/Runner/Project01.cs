﻿using System;
using DataStructures_Algorithms;
using DataStructures_Algorithms.Project1;

namespace Runner
{
	public class Project01 : IRunner
	{
		public Project01()
		{
		}

		public void Run(string[] args)
		{
			if (args.Length < 2)
			{
				Console.WriteLine("Expected two params");
				return;
			}

			string inputFilename = "../../Data/Project01/" + args[0];
			SortingAlgorithm sortingAlgorithm = (SortingAlgorithm) Enum.Parse(typeof(SortingAlgorithm), args[1]);
			string outputFilename = "../../Data/Project01/" + "S_" + args[0];

			Vector<int> vector = null;
			DataSerializer<int>.LoadVectorFromTextFile(inputFilename, ref vector);

			if (vector == null)
			{
				Console.WriteLine("Failed to load data from input file");
				return;
			}

			//let's check the capacity & count now
			Console.WriteLine("Vector Capacity is {0}", vector.Capacity);
			Console.WriteLine("Vector Count is {0}", vector.Count);


			//Let's sort Vector elements ascending?
			vector.Sort(sortingAlgorithm); //This is the same as calling vector.Sort with an ascending order comparer

			DataSerializer<int>.SaveVectorToTextFile(outputFilename, vector);

		}
	}
}

