﻿using System;
using System.Collections.Generic;

namespace DataStructures_Algorithms.Week04
{
	public class Queue<T>
	{
		IList<T> data = null;
		public Queue()
		{
			data = new LinkedList<T>();
		}
		public Queue(IList<T> datastore)
		{
			data = datastore;
		}

		public void Enque(T element)
		{
			//TODO: Implement Enque method

			data.Add(element);

			//throw new Exception("not implemented");
		}
		public T Deque()
		{
			//TODO: Implement Deque method
			//throw new Exception("not implemented");

			int index = data.Count - 1;
			T value = data[index];

			 data.RemoveAt(index);
			return value;

		}

		public int Count { get { return data.Count; } }
	}
}
