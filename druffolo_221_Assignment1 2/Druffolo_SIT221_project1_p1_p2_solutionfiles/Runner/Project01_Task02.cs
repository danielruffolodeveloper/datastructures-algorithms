﻿using System;
using DataStructures_Algorithms;
using DataStructures_Algorithms.Project1;

namespace Runner
{
	public class Project01_Task02 : IRunner
	{
		public Project01_Task02()
		{
		}

		public void Run(string[] args)
		{
			if (args.Length < 2)
			{
				Console.WriteLine("Expected at least two params");
				return;
			}

			string opName = args[0];
			OperationEnum op = (OperationEnum) Enum.Parse(typeof(OperationEnum), opName);




			string setAPath = "../../Data/Project01/" + args[1];
			SetClass<int> setB = null;
			int element = 0;

			if (op == OperationEnum.MEMBERSHIP)
				element = int.Parse(args[2]);
			else if (args.Length == 3)
			{
				string setBPath = "../../Data/Project01/" + args[2];
				Vector<int> setBData = null;
				DataSerializer<int>.LoadVectorFromTextFile(setBPath, ref setBData);


				if (setBData == null)
				{
					Console.WriteLine("Failed to load data from input file");
					return;
				}


				setB = new SetClass<int>(setBData);
			

			}
		



			Vector<int> setAData = null;
			DataSerializer<int>.LoadVectorFromTextFile(setAPath, ref setAData);

			if (setAData == null)
			{
				Console.WriteLine("Failed to load data from input file");
				return;
			}
			SetClass<int> setA = new SetClass<int>(setAData);



			switch (op)
			{
				case OperationEnum.MEMBERSHIP: 
					Console.WriteLine(string.Format("Check Membership of {0}, is {1}", element, setA.Membership(element))); break;


				case OperationEnum.SUBSET: 
					Console.WriteLine(string.Format("Check Subset of {0} in {1}, and result = {2}",setA.Data.ToString(),setB.Data.ToString(),setA.IsSubsetOf(setB))); break;
					
				case OperationEnum.SUPERSET:
					Console.WriteLine(string.Format("Check Superset of {0} in {1}, and result = {2}", setA.Data.ToString(), setB.Data.ToString(),setB.IsSupersetOf(setA)));
					break;

				case OperationEnum.POWERSET:
					Console.WriteLine(string.Format(setA.Powerset().Data.ToString()));
		
					break;

				case OperationEnum.INTERESECTION:
					Console.WriteLine(string.Format (setA.IntersectionWith(setB).Data.ToString()));
					break;

				case OperationEnum.UNION:
					Console.WriteLine(string.Format(setA.UnionWith(setB).Data.ToString()));
					break;

				case OperationEnum.SETDIFFERENCE:
					Console.WriteLine(string.Format(setA.Difference(setB).Data.ToString()));
					break;

				case OperationEnum.SYMETRICDIFFERENCE:
					Console.WriteLine(string.Format(setA.SymmetricDifference(setB).Data.ToString()));
					break;

				case OperationEnum.COMPLAMENT:
					Console.WriteLine(string.Format(""));
					break;

				case OperationEnum.CARTESIAN:

					Console.WriteLine(string.Format(setA.CartesianProduct(setB).Data.ToString()));

					break;




		
			
			}

		}
	}
}

