﻿using System;
using System.Collections.Generic;
using DataStructures_Algorithms;
using QuickGraph;

namespace Runner
{

	public class Runner07_Task01 : IRunner
	{
		public Runner07_Task01()
		{
		}

		public void Run(string[] args)
		{
			//TODO: please complete the run method


			ShoppingCenterGraph myshoppingcentergraph = new ShoppingCenterGraph();


			string result = "Adidas";

			foreach (PointOfInterest found in myshoppingcentergraph.FindPossibleRoutesFrom(result))
			{
				Console.WriteLine(found.Name);

			}

			 result = "Nike";

			foreach (PointOfInterest found in myshoppingcentergraph.FindPossibleRoutesFrom(result))
			{
				Console.WriteLine(found.Name);

			}

		
			result = "Woolworths";

			foreach (PointOfInterest found in myshoppingcentergraph.FindPossibleRoutesFrom(result))
			{
				Console.WriteLine(found.Name);

			}


			result = "Coles";

			foreach (PointOfInterest found in myshoppingcentergraph.FindPossibleRoutesFrom(result))
			{
				Console.WriteLine(found.Name);

			}


			result = "EB Games";

			foreach (PointOfInterest found in myshoppingcentergraph.FindPossibleRoutesFrom(result))
			{
				Console.WriteLine(found.Name);

			}

			result = "JBHIFI";

			foreach (PointOfInterest found in myshoppingcentergraph.FindPossibleRoutesFrom(result))
			{
				Console.WriteLine(found.Name);

			}

			}
	}
}
