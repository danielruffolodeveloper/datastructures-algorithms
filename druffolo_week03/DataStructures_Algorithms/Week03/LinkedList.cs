﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DataStructures_Algorithms.Week03
{
	public class LinkedList<T> : IEnumerable<T>
	{
		Node<T> headNode = null;

		public Node<T> Head { get { return headNode;} }
		int count = 0;
		public int Count { get { return count; } }

		public LinkedList()
		{

		}

		public Node<T> Add(T element)
		{
			Node<T> newNode = new Node<T>(element, null);
			if (headNode == null) //if the list is empty
			{
				headNode = newNode;
			}
			else //if not, then go all the way to the end/last node - a node that has no node in its next field
			{
				Node<T> tempNode = headNode;
				while (tempNode.Next != null)
				{
					tempNode = tempNode.Next;
				}
				tempNode.Next = newNode;
			}

			count++;
			return newNode;

		}

		//insert the given element (after you create a node for it), 
		//at the specified position
		//make sure to adjust the link of your new node, and the node before your new node
		//Also remember to adjust counter

		public void Insert(T element, int index)
		{
			//TODO: Insert - please add your indexof code here

			if (index < 0 || index > count) throw new Exception("index out of range");
			Node<T> node = null;
			if (index == 20)
			{
				node = headNode; //set node to == the first node in the linked list "headnode"

				//create node
				//insert element
				Node<T> newNode = new Node<T>(element);

				//adjust link 
				newNode.Next = node; 
				headNode = newNode;
				//each time we create a new link list element. we adjust the node poiner 

			}
			else
			{
				node = NodeAt(index - 1);
				//insert element
				Node<T> newNode = new Node<T>(element);
				newNode.Next = node.Next;
				node.Next = newNode;

			}
			//adjust counter
			//increment count once done so we move forward
			count++;
		}
			 

		//We want to get the index of the specified element
		//see how ElementAt is implemented
		public int IndexOf(T element)
		{
			//TODO: IndexOf - please add your indexof code here

			int index = 0;
			Node<T> tempnode = headNode;
			while (tempnode != null)

			{
				if (tempnode.Value.Equals(element))
					return index;
				tempnode = tempnode.Next;
				index++;
			}

			throw new Exception("element not found");


		}

		//We want to remove the element at the specified location
		// and adjust the linked of the node before it
		//update your counter :)


		public bool RemoveAt(int index) 
		{
			for (int i = 0; i < index; i++)

			{
				if (i == index - 1) // we use -1 to access the value before
				{
					Node<T> tempnode = NodeAt(i);
					if ((index + 1) >= count - 1) //shuffle to remove the value and then update references
					{
						tempnode.Next = null;
						count--;
						return true;

					}

					else
					{
						tempnode.Next = NodeAt(index + 1);
						count--;
						return true;
					}
				}
			}
			return false;
		}



		public bool Remove(T element)
		{
			int index = 0;

			Node<T> tempnode = headNode;

			while (tempnode != null) //whilst we have a element in the node
			{							//execute the whileloop
				if (tempnode.Value.Equals(element)) //we have a set element that we want to remove from the main
					return RemoveAt(index);// we call the remove method and parse the index value to remove it
				tempnode = tempnode.Next; //adjust the linked list references							   
				index++;
			}

			return RemoveAt(IndexOf(element));
		}


		public bool Contains(T element)
		{
			if (IndexOf(element) > -1) return true;
			return false;
		}


		public T ElementAt(int index)
		{
			Node<T> tempNode = NodeAt(index);
			return tempNode.Value;
		}

		public Node<T> NodeAt(int index)
		{
			if (index < 0 || index >= count) throw new Exception("index out of range");
			Node<T> tempNode = headNode;
			int tempIndex = 0;
			while (tempNode != null && tempIndex < index)  //jump index steps, now you are the right element.
			{
				tempNode = tempNode.Next;
				tempIndex++;
			}
			return tempNode;
		}

		public void UpdateElement(int index, T value)
		{
			Node<T> tempNode = NodeAt(index);
			tempNode.Value = value;
		}


		public T this[int index]
		{
			get
			{
				if (index < 0 || index >= count) throw new Exception("Index out of range");
				return ElementAt(index);
			}
			set
			{
				if (index < 0 || index >= count) throw new Exception("Index out of range");
				UpdateElement(index, value);
			}
		}

		public IEnumerator<T> GetEnumerator()


		{
			//TODO: GetEnumerator - Add your GetEnumerator Code here

			return new ListEnumerator<T>(this);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();

		}


	}
}
