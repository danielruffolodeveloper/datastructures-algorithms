﻿using System;
using System.Collections.Generic;
using System.Text;
using DataStructures_Algorithms.Project1;

namespace DataStructures_Algorithms
{
	public class SetClass<T>
	{
		public Vector<T> Data 
		{
			get; 
			set;
		}

		public bool Membership(T element) {
			return Data.Contains(element);

		}

		public bool IsSubsetOf(SetClass<T> B) 

		{
			for (int i = 0; i < Data.Count; i++)
			{
				if (B.Data.Contains(Data[i]) == false) return false;
			}
			return true;
		}

		public bool IsSupersetOf(SetClass<T> B) 

		// DATA [i] is data in set a

		//if all elements in B belong to A
		{
			for (int i = 0; i < Data.Count; i++)
				
			{
				if (B.Data.Contains(Data[i]) == false) return false;
			}
			return true;
		}


		public SetClass<SetClass<T>> Powerset() 

		{
			int n = Data.Count;
			int powerSetCount = 1 << n;
		

			Vector<SetClass<T>> templist = new Vector<SetClass<T>>();

			for (int setM = 0; setM < powerSetCount; setM++)
			{
				Vector<T> Subset = new Vector<T>();

				for (int i = 0; i < n; i++)
				{
					// Checking if element of collection should be added

					if ((setM & (1 << i)) > 0)
					{
						Subset.Add(Data[i]);
					}
				}
				templist.Add(new SetClass<T>(Subset)); 
		}

			SetClass<SetClass<T>> outputPs = new SetClass<SetClass<T>>(templist);
			return outputPs ;
		}

		public override string ToString()
		{
			return string.Format("{0}\n", Data);

		}

		public SetClass<T> IntersectionWith(SetClass<T> B)

		{
			Vector<T> templist = new Vector<T>();

			for (int i = 0; i < Data.Count; i++)
			{
				if (B.Data.Contains(Data[i]) == true) templist.Add(Data[i]);
			}

			SetClass<T> intersectVals = new SetClass<T>(new Vector<T>());
			intersectVals.Data = templist;
			return intersectVals;

		}


		public SetClass<T> UnionWith(SetClass<T> B) 

		{
			Vector<T> templist = new Vector<T>();
			templist = Data;

			for (int i = 0; i < B.Data.Count; i++)

				if (templist.Contains(B.Data[i]) == false) templist.Add(B.Data[i]);

		
			SetClass<T> unionVals = new SetClass<T>(new Vector<T>());
			unionVals.Data = templist;
			return unionVals;


		}
	

		public SetClass<T> Difference(SetClass<T> B)
		{
			Vector<T> templist = new Vector<T>();
			//our temp list is for our set a data
			templist = Data;

			for (int i = 0; i < B.Data.Count; i++)

				if (templist.Contains(B.Data[i]) == true) 

					templist.Remove(B.Data[i]);


			SetClass<T> differenceVals = new SetClass<T>(new Vector<T>());
			differenceVals.Data = templist;
			return differenceVals;
		}

		public SetClass<T> SymmetricDifference(SetClass<T> B) 
		{ 
			Vector<T> templist = new Vector<T>();
			//our temp list is for our set a data
			templist = Data;

			for (int i = 0; i < B.Data.Count; i++)

				if (templist.Contains(B.Data[i]) == true)
					templist.Remove(B.Data[i]);
			
				else if (templist.Contains(B.Data[i]) == false)
					templist.Add(B.Data[i]);



			SetClass<T> sdifferenceVals = new SetClass<T>(new Vector<T>());
			sdifferenceVals.Data = templist;
			return sdifferenceVals;
		}

		public SetClass<T> Complement(SetClass<T> U)
		{
			
			return null;
		}

		public SetClass<Tuple<T, T2>> CartesianProduct<T2>(SetClass<T2> B)

		{
			SetClass<Tuple<T, T2>> result = new SetClass<Tuple<T, T2>>(new Vector<Tuple<T, T2>>());
			for (int i = 0; i < Data.Count; i++)
			{
				for (int x = 0; x < B.Data.Count; x++)
				{
					Tuple<T, T2> current = new Tuple<T, T2>(Data[i], B.Data[x]);
					result.Data.Add(current);
				}
			}
			return result;

		}

		public SetClass(Vector<T> d)
		{
			Data = d;
		}
	}
}

