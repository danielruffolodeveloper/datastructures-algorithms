﻿using System;
using System.Diagnostics;
using System.Threading;
using DataStructures_Algorithms;
using DataStructures_Algorithms.Project1;

namespace Runner
{
	public class Project01_Task01 : IRunner
	{
		public Project01_Task01()
		{
		}

		public void Run(string[] args)
		{
			if (args.Length < 2)
			{
				Console.WriteLine("Expected two params");
				return;
			}

			string inputFilename = "../../Data/Project01/" + args[0];
			SortingAlgorithm sortingAlgorithm = (SortingAlgorithm) Enum.Parse(typeof(SortingAlgorithm), args[1]);
			string outputFilename = "../../Data/Project01/" + "S_" + args[1] + "_A_" + args[0];

			Vector<int> vector = null;
			DataSerializer<int>.LoadVectorFromTextFile(inputFilename, ref vector);

			if (vector == null)
			{
				Console.WriteLine("Failed to load data from input file");
				return;
			}

			//let's check the capacity & count now
			Console.WriteLine("Vector Capacity is {0}", vector.Capacity);
			Console.WriteLine("Vector Count is {0}", vector.Count);


			//Let's sort Vector elements ascending?

			var beforeGC = Process.GetCurrentProcess().WorkingSet64;
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();

			var memBefore = Process.GetCurrentProcess().WorkingSet64;
			Stopwatch s = new Stopwatch();

			s.Start();
			vector.Sort(sortingAlgorithm);
			s.Stop();

			var memAfter = Process.GetCurrentProcess().WorkingSet64;


			Console.WriteLine("RAW execution time:" + s.Elapsed);

			Console.WriteLine("execution time in miliseconds:" + s.ElapsedMilliseconds);
			Console.WriteLine("memory usage:"+ (memAfter - memBefore) / 1024.0) ;


			Console.WriteLine("Time elapsed (seconds): {0}", s.Elapsed.TotalSeconds);
			Console.WriteLine("Time elapsed (miliseconds): {0}", s.Elapsed.TotalMilliseconds);
			Console.WriteLine("Time elapsed (nanoseconds): {0}", s.Elapsed.TotalMilliseconds * 1000000);


		    var afterGC = Process.GetCurrentProcess().WorkingSet64;

			Console.WriteLine("memory removed by GC =" + (afterGC - beforeGC) / 1024.0);

			DataSerializer<int>.SaveVectorToTextFile(outputFilename, vector);
		

		}
	}
}

