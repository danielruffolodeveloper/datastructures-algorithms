﻿using System;
using System.Collections.Generic;
            
namespace DataStructures_Algorithms
{
	public class ShoppingCenter
	{

		Dictionary<string, PointOfInterest> POIsTable 

		{ get; set; }




		//shopping center constructor

		public ShoppingCenter()
		{
			POIsTable = new Dictionary<string, PointOfInterest>();


			//point of interest 1

			PointOfInterest adidas = new PointOfInterest();
			adidas.Name = "adidas";
			adidas.Description = " a shop for sports gear, clothing and accessories ";
			adidas.Location = " Ground Level";

			List <string> adidasServices = new List<string>();
			adidasServices.Add("Footware");
			adidasServices.Add("Clothing");
			adidasServices.Add("Sports Gear");

			adidas.Services = adidasServices;

			POIsTable.Add("adidas",adidas);


			//point of interest 2

			PointOfInterest EBGames = new PointOfInterest();

			EBGames.Name = "EB games";
			EBGames.Description = " a shop for selling video games ";
			EBGames.Location = "Level 1";

			List<string> EBServices = new List<string>();
			EBServices.Add("Games");
			EBServices.Add("Pre owned");
			EBServices.Add("Trade Ins");

			EBGames.Services = EBServices;
			POIsTable.Add("EB games", EBGames);


			//point of interest 3

			PointOfInterest Target = new PointOfInterest();

			Target.Name = "Target";
			adidas.Description = " a shop for selling clothing,homewares,ect ";
			adidas.Location = " Second Level";

			List<string> Targetservices = new List<string>();
			Targetservices.Add("homewares");
			Targetservices.Add("mens clothing");
			Targetservices.Add("womans clothing");

			Target.Services = Targetservices;
			POIsTable.Add("Target", Target);

			//point of interest 4

			PointOfInterest Kmart = new PointOfInterest();

			Target.Name = "Kmart";
			adidas.Description = " a shop for selling clothing,homewares,ect ";
			adidas.Location = " first Level";

			List<string> Kmartservices = new List<string>();
			Kmartservices.Add("Home Wares");
			Kmartservices.Add("Car and Auto");
			Kmartservices.Add("Clothing");

			Kmart.Services = Kmartservices;
			POIsTable.Add("Kmart", Kmart);

			//point of interest 5

			PointOfInterest PetsGalore = new PointOfInterest();

			PetsGalore.Name = "PetsGalore";
			PetsGalore.Description = " a shop for finding a new fury friend ";
			PetsGalore.Location = " Second Level";

			List<string> PetsGaloreservices = new List<string>();
			PetsGaloreservices.Add("Pets");
			PetsGaloreservices.Add("groming");
			PetsGaloreservices.Add("pet food");

			PetsGalore.Services = PetsGaloreservices;
			POIsTable.Add("PetsGalore", PetsGalore);

			//point of interest 6

			PointOfInterest ToysRUs = new PointOfInterest();

			ToysRUs.Name = "Toys R Us";
			ToysRUs.Description = " a shop for selling everything related to toys ";
			ToysRUs.Location = " Second Level";

			List<string> ToysRUsservices = new List<string>();
			ToysRUsservices.Add("toys");
			ToysRUsservices.Add("baby furnature");
			ToysRUsservices.Add("bikes");

			ToysRUs.Services = Targetservices;
			POIsTable.Add("ToysRUs", ToysRUs);

			//point of interest 7

			PointOfInterest JBHIFI = new PointOfInterest();

			JBHIFI.Name = "JBHIFI";
			JBHIFI.Description = " a shop for selling electronics ";
			JBHIFI.Location = " first Level";

			List<string> JBHIFIservices = new List<string>();
			JBHIFIservices.Add("Games");
			JBHIFIservices.Add("Music");
			JBHIFIservices.Add("TV's");
			JBHIFIservices.Add("Comupters");

			JBHIFI.Services = JBHIFIservices;
			POIsTable.Add("JBHIFI", JBHIFI);

			//point of interest 8

			PointOfInterest Coles = new PointOfInterest();

			Coles.Name = "Coles";
			Coles.Description = " a shop for selling groceries ";
			Coles.Location = " Second Level";

			List<string> Colesservices = new List<string>();
			Colesservices.Add("Fruit/vegitables");
			Colesservices.Add("Frozen");
			Colesservices.Add("Bakery");

			Coles.Services = Colesservices;
			POIsTable.Add("Coles", Coles);

			//point of interest 9

			PointOfInterest Woolworths = new PointOfInterest();

			Woolworths.Name = "Woolworths";
			Woolworths.Description = " a shop for selling food, fruit ,vegitables,ect ";
			Woolworths.Location = " Second Level";

			List<string> Woolworthsservices = new List<string>();
			Woolworthsservices.Add("Fruit/vegitables");
			Woolworthsservices.Add("Frozen");
			Woolworthsservices.Add("Bakery");

			Woolworths.Services = Woolworthsservices;
			POIsTable.Add("Woolworths", Woolworths);

			//point of interest 10

			PointOfInterest Myer = new PointOfInterest();

			Myer.Name = "Myer";
			Myer.Description = " a shop for selling designer clothing,homewares,ect ";
			Myer.Location = " first level ";

			List<string> Myerservices = new List<string>();
			Myerservices.Add("mens clothing");
			Myerservices.Add("womans clothing");
			Myerservices.Add("homewares");

			Myer.Services = Myerservices;
			POIsTable.Add("Myer", Myer);





		}



		public string SearchByPOIName(string POIName)
		{
			if (POIsTable.ContainsKey(POIName)) return POIsTable[POIName].ToString();
			else
			{
				return "POI DOESNT EXIST";
			}
		}


		public string searchbyservice (string servicename)
		{
			if (POIsTable.ContainsKey(servicename)) return POIsTable[servicename].ToString();
			else
			{
				return "SERVICE DOESNT EXIST";
			}
		}
	}
}

