﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using QuickGraph;
using QuickGraph.Algorithms.Observers;
using System.Collections;

namespace DataStructures_Algorithms
{
	public class BackendService
	{
		//TODO: Declare LocationServiceQueue (public property & field)
		//This is where device messages will be enqueued. 
		//The type of this data structure need to be ConcurrentQueue<DeviceMessage>
		public ConcurrentQueue<DeviceMessage> LocationServiceQueue { get; set; }


		//TODO: Declare POIsTable (public property & field), this table is used to maintain POI information
		//Key is string (POI Name), and value is POI object
		//The type of this data structure need to be Dictionary<string, POI>        

		public Dictionary<string, POI> POIsTable { get; set; }


		//TODO: Declare POIsGraph (public property & field), this is a Bidirectional Graph between all POIs in the shopping center
		//Nodes of type POI, and Edges of type Edge<POI>
		//The type of this data structure need to be BidirectionalGraph<POI, Edge<POI>>
		public BidirectionalGraph<POI, Edge<POI>> POIsGraph { get; set; }
		//TODO: Declare ActiveDevicesTable (public property & field), this is a dictionary of active devices and their navigation details
		//The type of this data structure needs to be ConcurrentDictionary<string, NavigationDetails>

		public ConcurrentDictionary<string, NavigationDetails> ActiveDevicesTable { get; set; }

		public int NumberOfPOIs
		{
			get { return POIsTable.Count; }

		}

		public bool ShoppingCenterIsOpen { get; set; }


		public bool FindPOI(string DeviceId, POI Source, POI Target)
		{

			Stack<Edge<POI>> path = GetShortestPath(Source, Target);
			if (path == null) return false;

			NavigationDetails current = new NavigationDetails();
			current.CurrentPOI = Source;
			current.DestinationPOI = Target;
			current.PathToDestination = path;
			ActiveDevicesTable.AddOrUpdate(DeviceId, id => current, (id, value) => { value = current; return value; });

			return true;

		}
		public BackendService()
		{

		}
		public void Init()
		{
			//TODO: Initialise your POIsTable
			POIsTable = new Dictionary<string, POI>();
			LocationServiceQueue = new ConcurrentQueue<DeviceMessage>();

			//TODO: Intialise your POIsGraph
			POIsGraph = new BidirectionalGraph<POI, Edge<POI>>();

			//TODO: Add a list of POI locations (at least 10 POIs) to the POIsTable & to the POIsGraph
			//Many lines here for POIs

			/*
			var McDonalds = new POI { POIName = "McDonalds", POIDescription = "Food" };
			var ToysRUs = new POI { POIName = "ToysRUs", POIDescription = "Toys" };
			var Myer = new POI { POIName = "Myer", POIDescription = "Clothes" };
			var Nike = new POI { POIName = "Nike", POIDescription = "Clothes" };
			var Adidas = new POI { POIName = "Adidas", POIDescription = "Clothes" };
			var Safeway = new POI { POIName = "Safeway", POIDescription = "Food" };
			var Coles = new POI { POIName = "Coles", POIDescription = "Food" };
			var KFC = new POI { POIName = "KFC", POIDescription = "Food" };
			var Aldi = new POI { POIName = "Aldi", POIDescription = "Food" };
			var DonutKing = new POI { POIName = "DonutKing", POIDescription = "Food" };

			POIsTable.Add("McDonalds", McDonalds);
			POIsTable.Add("ToysRUs", ToysRUs);
			POIsTable.Add("Myer", Myer);
			POIsTable.Add("Nike", Nike);
			POIsTable.Add("Adidas", Adidas);
			POIsTable.Add("Safeway", McDonalds);
			POIsTable.Add("Coles", Coles);
			POIsTable.Add("KFC", KFC);
			POIsTable.Add("Aldi", Aldi);
			POIsTable.Add("DonutKing", DonutKing);

			POIsGraph.AddVertex(McDonalds);
			POIsGraph.AddVertex(ToysRUs);
			POIsGraph.AddVertex(Myer);
			POIsGraph.AddVertex(Nike);
			POIsGraph.AddVertex(Adidas);
			POIsGraph.AddVertex(Safeway);
			POIsGraph.AddVertex(Coles);
			POIsGraph.AddVertex(KFC);
			POIsGraph.AddVertex(Aldi);
			POIsGraph.AddVertex(DonutKing);


			//TODO: Add edges to your POIsGraph (20 edges? more? up to you)
			//Many lines here for edge


			POIsGraph.AddEdge(new Edge<POI> { Source = McDonalds, Target = ToysRUs, Description = "McDonalds to ToysRUs, Please Walk Straight", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = ToysRUs, Target = Myer, Description = "ToysRUs to Myer", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = ToysRUs, Target = DonutKing, Description = "ToysRUs to DonutKing", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = ToysRUs, Target = Nike, Description = "ToysRUs to Nike", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = ToysRUs, Target = McDonalds, Description = "ToysRUs to McDonalds", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = Myer, Target = ToysRUs, Description = "Myer to ToysRUs", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = Myer, Target = Adidas, Description = "Myer to Adidas", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = Adidas, Target = Nike, Description = "Adidas to Nike", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = Adidas, Target = Myer, Description = "Adidas to Myer", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = Nike, Target = Safeway, Description = "Nike to Safeway", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = Nike, Target = ToysRUs, Description = "Nike to ToysRUs", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = DonutKing, Target = Safeway, Description = "DonutKing to Safeway", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = DonutKing, Target = Aldi, Description = "DonutKing to Aldi", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = DonutKing, Target = ToysRUs, Description = "DonutKing to ToysRUs", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = Aldi, Target = KFC, Description = "Aldi to KFC", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = Aldi, Target = DonutKing, Description = "Aldi to DonutKing", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = Safeway, Target = Coles, Description = "Safeway to Coles", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = Safeway, Target = KFC, Description = "Safeway to KFC", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = Safeway, Target = Nike, Description = "Safeway to Nike", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = KFC, Target = Safeway, Description = "KFC to Safeway", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = KFC, Target = Coles, Description = "KFC to Coles", Distance = 5 });
			POIsGraph.AddEdge(new Edge<POI> { Source = KFC, Target = Aldi, Description = "KFC to Aldi", Distance = 5 });
*/


			/////////////////////////////////////////////////////////////////////////////
			//point of interest 1

			POI Adidas = new POI();
			Adidas.POIName = "Adidas";
			Adidas.POIDescription = " a shop for sports gear, clothing and accessories ";
			Adidas.POILocation = " Ground Level";

			List<string> AdidasServices = new List<string>();
			AdidasServices.Add("Footware");
			AdidasServices.Add("Clothing");
			AdidasServices.Add("Sports Gear");

			Adidas.Services = AdidasServices;

			POIsTable.Add("Adidas", Adidas);
			POIsGraph.AddVertex(Adidas);

			/////////////////////////////////////////////////////////////////////////////
			//point of interest 2
			////////////////////////////////////////////////////////////////////////////

			POI EBGames = new POI();

			EBGames.POIName = "EB games";
			EBGames.POIDescription = " a shop for selling video games ";
			EBGames.POILocation = "Level 1";

			List<string> EBServices = new List<string>();
			EBServices.Add("Games");
			EBServices.Add("Pre owned");
			EBServices.Add("Trade Ins");

			EBGames.Services = EBServices;
			POIsTable.Add("EB games", EBGames);
			POIsGraph.AddVertex(EBGames);


			/////////////////////////////////////////////////////////////////////////////
			//point of interest 3
			/////////////////////////////////////////////////////////////////////////////

			POI Target = new POI();

			Target.POIName = "Target";
			Adidas.POIDescription = " a shop for selling clothing,homewares,ect ";
			Adidas.POILocation = " Second Level";

			List<string> Targetservices = new List<string>();
			Targetservices.Add("homewares");
			Targetservices.Add("mens clothing");
			Targetservices.Add("womans clothing");

			Target.Services = Targetservices;
			POIsTable.Add("Target", Target);
			POIsGraph.AddVertex(Target);

			/////////////////////////////////////////////////////////////////////////////
			//point of interest 4
			/////////////////////////////////////////////////////////////////////////////

			POI Kmart = new POI();

			Kmart.POIName = "Kmart";
			Kmart.POIDescription = " a shop for selling clothing,homewares,ect ";
			Kmart.POILocation = " first Level";

			List<string> Kmartservices = new List<string>();
			Kmartservices.Add("Home Wares");
			Kmartservices.Add("Car and Auto");
			Kmartservices.Add("Clothing");

			Kmart.Services = Kmartservices;
			POIsTable.Add("Kmart", Kmart);
			POIsGraph.AddVertex(Kmart);

			/////////////////////////////////////////////////////////////////////////////
			//point of interest 5
			/////////////////////////////////////////////////////////////////////////////

			POI PetsGalore = new POI();

			PetsGalore.POIName = "PetsGalore";
			PetsGalore.POIDescription = " a shop for finding a new fury friend ";
			PetsGalore.POILocation = " Second Level";

			List<string> PetsGaloreservices = new List<string>();
			PetsGaloreservices.Add("Pets");
			PetsGaloreservices.Add("groming");
			PetsGaloreservices.Add("pet food");

			PetsGalore.Services = PetsGaloreservices;
			POIsTable.Add("PetsGalore", PetsGalore);
			POIsGraph.AddVertex(PetsGalore);


			/////////////////////////////////////////////////////////////////////////////
			//point of interest 6
			/////////////////////////////////////////////////////////////////////////////

			POI ToysRUs = new POI();

			ToysRUs.POIName = "Toys R Us";
			ToysRUs.POIDescription = " a shop for selling everything related to toys ";
			ToysRUs.POILocation = " Second Level";

			List<string> ToysRUsservices = new List<string>();
			ToysRUsservices.Add("toys");
			ToysRUsservices.Add("baby furnature");
			ToysRUsservices.Add("bikes");

			ToysRUs.Services = Targetservices;
			POIsTable.Add("ToysRUs", ToysRUs);
			POIsGraph.AddVertex(ToysRUs);


			/////////////////////////////////////////////////////////////////////////////
			//point of interest 7
			/////////////////////////////////////////////////////////////////////////////

			POI JBHIFI = new POI();

			JBHIFI.POIName = "JBHIFI";
			JBHIFI.POIDescription = " a shop for selling electronics ";
			JBHIFI.POILocation = " first Level";

			List<string> JBHIFIservices = new List<string>();
			JBHIFIservices.Add("Games");
			JBHIFIservices.Add("Music");
			JBHIFIservices.Add("TV's");
			JBHIFIservices.Add("Comupters");

			JBHIFI.Services = JBHIFIservices;
			POIsTable.Add("JBHIFI", JBHIFI);
			POIsGraph.AddVertex(JBHIFI);

			/////////////////////////////////////////////////////////////////////////////
			//point of interest 8
			/////////////////////////////////////////////////////////////////////////////

			POI Coles = new POI();

			Coles.POIName = "Coles";
			Coles.POIDescription = " a shop for selling groceries ";
			Coles.POILocation = " Second Level";

			List<string> Colesservices = new List<string>();
			Colesservices.Add("Fruit/vegitables");
			Colesservices.Add("Frozen");
			Colesservices.Add("Bakery");

			Coles.Services = Colesservices;
			POIsTable.Add("Coles", Coles);
			POIsGraph.AddVertex(Coles);

			/////////////////////////////////////////////////////////////////////////////
			//point of interest 9
			/////////////////////////////////////////////////////////////////////////////

			POI Woolworths = new POI();

			Woolworths.POIName = "Woolworths";
			Woolworths.POIDescription = " a shop for selling food, fruit ,vegitables,ect ";
			Woolworths.POILocation = " Second Level";

			List<string> Woolworthsservices = new List<string>();
			Woolworthsservices.Add("Fruit/vegitables");
			Woolworthsservices.Add("Frozen");
			Woolworthsservices.Add("Bakery");

			Woolworths.Services = Woolworthsservices;
			POIsTable.Add("Woolworths", Woolworths);
			POIsGraph.AddVertex(Woolworths);

			/////////////////////////////////////////////////////////////////////////////
			//point of interest 10
			/////////////////////////////////////////////////////////////////////////////

			POI Myer = new POI();

			Myer.POIName = "Myer";
			Myer.POIDescription = " a shop for selling designer clothing,homewares,ect ";
			Myer.POILocation = " first level ";

			List<string> Myerservices = new List<string>();
			Myerservices.Add("mens clothing");
			Myerservices.Add("womans clothing");
			Myerservices.Add("homewares");

			Myer.Services = Myerservices;

			POIsTable.Add("Myer", Myer);
			POIsGraph.AddVertex(Myer);




			//_______________________________________________________________________
			//TODO: Add Edges to your graph, using POIGraph.AddEdge( give it a new edge that links between two verticies)
			//Make sure to set Source/Target/Description and Distance

			//_______________________________________________________________________

			/////////////////////////////
			//1 adidas
			/////////////////////////////
			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Adidas,
				Target = EBGames,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Adidas,
				Target = Target,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Adidas,
				Target = Kmart,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Adidas,
				Target = PetsGalore,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Adidas,
				Target = ToysRUs,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Adidas,
				Target = JBHIFI,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Adidas,
				Target = Coles,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Adidas,
				Target = Woolworths,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Adidas,
				Target = Myer,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});
			/////////////////////////////
			//2 eb games
			/////////////////////////////


			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = EBGames,
				Target = Target,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = EBGames,
				Target = Kmart,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = EBGames,
				Target = PetsGalore,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = EBGames,
				Target = ToysRUs,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = EBGames,
				Target = JBHIFI,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = EBGames,
				Target = Coles,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = EBGames,
				Target = Woolworths,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = EBGames,
				Target = Myer,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});
			/////////////////////////////
			//3 Target
			/////////////////////////////
			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Target,
				Target = EBGames,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});



			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Target,
				Target = Kmart,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Target,
				Target = PetsGalore,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Target,
				Target = ToysRUs,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Target,
				Target = JBHIFI,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Target,
				Target = Coles,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Target,
				Target = Woolworths,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Target,
				Target = Myer,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});
			/////////////////////////////
			//4 Kmart
			/////////////////////////////
			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Kmart,
				Target = EBGames,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Kmart,
				Target = Target,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});



			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Kmart,
				Target = PetsGalore,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Kmart,
				Target = ToysRUs,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Kmart,
				Target = JBHIFI,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Kmart,
				Target = Coles,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Kmart,
				Target = Woolworths,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Kmart,
				Target = Myer,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});
			/////////////////////////////
			//5 Pets galore
			/////////////////////////////
			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = PetsGalore,
				Target = EBGames,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = PetsGalore,
				Target = Target,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = PetsGalore,
				Target = Kmart,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});



			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = PetsGalore,
				Target = ToysRUs,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = PetsGalore,
				Target = JBHIFI,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = PetsGalore,
				Target = Coles,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = PetsGalore,
				Target = Woolworths,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = PetsGalore,
				Target = Myer,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});
			/////////////////////////////
			//6 ToysRus
			/////////////////////////////
			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = ToysRUs,
				Target = EBGames,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = ToysRUs,
				Target = Target,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = ToysRUs,
				Target = Kmart,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = ToysRUs,
				Target = PetsGalore,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});



			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = ToysRUs,
				Target = JBHIFI,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = ToysRUs,
				Target = Coles,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = ToysRUs,
				Target = Woolworths,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = ToysRUs,
				Target = Myer,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});
			/////////////////////////////
			//7 JBHIFI
			/////////////////////////////
			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = JBHIFI,
				Target = EBGames,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = JBHIFI,
				Target = Target,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = JBHIFI,
				Target = Kmart,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = JBHIFI,
				Target = PetsGalore,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = JBHIFI,
				Target = ToysRUs,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});


			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = JBHIFI,
				Target = Coles,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = JBHIFI,
				Target = Woolworths,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = JBHIFI,
				Target = Myer,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});
			/////////////////////////////
			//8 Coles
			/////////////////////////////
			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Coles,
				Target = EBGames,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Coles,
				Target = Target,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Coles,
				Target = Kmart,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Coles,
				Target = PetsGalore,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Coles,
				Target = ToysRUs,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Coles,
				Target = JBHIFI,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});



			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Coles,
				Target = Woolworths,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Coles,
				Target = Myer,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});
			/////////////////////////////
			//9 Woolworths
			/////////////////////////////
			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Woolworths,
				Target = EBGames,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Woolworths,
				Target = Target,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Woolworths,
				Target = Kmart,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Woolworths,
				Target = PetsGalore,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Woolworths,
				Target = ToysRUs,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Woolworths,
				Target = JBHIFI,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Woolworths,
				Target = Coles,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});


			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Woolworths,
				Target = Myer,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});
			/////////////////////////////
			//10 Myer
			/////////////////////////////
			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Myer,
				Target = EBGames,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Myer,
				Target = Target,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Myer,
				Target = Kmart,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Myer,
				Target = PetsGalore,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Myer,
				Target = ToysRUs,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Myer,
				Target = JBHIFI,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Myer,
				Target = Coles,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});

			POIsGraph.AddEdge(new Edge<POI>
			{
				Source = Myer,
				Target = Woolworths,
				Distance = 20,
				Description = " (TRAVELED FROM) Adidas (TO) ----------> Nike"
				//_______________________________________________________________________
				//_______________________________________________________________________

			});


			/////////////////////////////



			//TODO: Initialise your ActiveDevicesTable, this is an empty table - to be filled automatically
			ActiveDevicesTable = new ConcurrentDictionary<string, NavigationDetails>();

			//Now let's open the shopping center and create a queue handling thread.
			//You do not need to modify this code
			ShoppingCenterIsOpen = true;
			Thread queueHandler = new Thread(() => this.WatchLocationServiceQueue());
			queueHandler.Start();
		}


		void WatchLocationServiceQueue()
		{


			DeviceMessage message = null;
			while (ShoppingCenterIsOpen == true && LocationServiceQueue.TryDequeue(out message) == false) { }
			if (ShoppingCenterIsOpen == false) return;

			if (ActiveDevicesTable.ContainsKey(message.DeviceId))
			{
				NavigationDetails current = new NavigationDetails();
			}

			//TODO: Complete the WatchLocationServiceQueue method
			//At this point (line), you have a message you retrieved from the queue - this is the message object
			//If the message is sent by a known device (exists in the ActiveDevicesTable, then we need to update route & give directions
			//Otherwise -else skip the message, in a future version, you may track people movement in the shoppping center (not now)


			//If the device known - exists in the ActiveDevicesTable, then we need to check if there is any step left in 
			//the PathToDestination stack - you can use count > 0
			//If yes then we need to Pop an edge from the stack


			if (ActiveDevicesTable.ContainsKey(message.DeviceId))
				if (ActiveDevicesTable[message.DeviceId].PathToDestination.Count > 0)

			
			{ 
					var newpoi = ActiveDevicesTable[message.DeviceId].PathToDestination.Pop();

					if (newpoi.Source == ActiveDevicesTable[message.DeviceId].CurrentPOI) 
				{
					ActiveDevicesTable[message.DeviceId].CurrentPOI = newpoi.Target;
				
				}
										
						ActiveDevicesTable[message.DeviceId].CurrentPOI = newpoi.Source;

				}







			// and then set the CurrentPOI of this device entry in the ActiveDevicesTable 
			// to either Source or Target (based on route direction)
			//This step is important, because I wait for the value of the CurrentPOI to change 
			//in the Walk method (see the Runner class)
			//To tell the user that there is a new direction 

			//This will make sure that we process the next item in the LocationServiceQueue
			WatchLocationServiceQueue();
		}


		public Stack<Edge<POI>> GetShortestPath(POI Source, POI Target)
		{
			QuickGraph.Algorithms.ShortestPath.DijkstraShortestPathAlgorithm<POI, Edge<POI>> algo =
				new QuickGraph.Algorithms.ShortestPath.DijkstraShortestPathAlgorithm<POI, Edge<POI>>(POIsGraph, (Edge<POI> arg) => arg.Distance);


			// creating the observer & attach it
			var vis = new VertexPredecessorRecorderObserver<POI, Edge<POI>>();
			vis.Attach(algo);

			// compute and record shortest paths
			algo.Compute(Target);

			// this can create all the shortest path in the graph
			IEnumerable<Edge<POI>> path = null;
			vis.TryGetPath(Source, out path);
			Stack<Edge<POI>> pathStack = new Stack<Edge<POI>>();
			if (path == null) return null;

			foreach (Edge<POI> e in path)
				pathStack.Push(e);
			return pathStack;
		}



	}
}