﻿using System;
namespace DataStructures_Algorithms
{
	public enum OperationEnum
	{
		MEMBERSHIP,
		INTERESECTION,
		UNION,
		POWERSET,
		SUBSET,
		SUPERSET,
		SETDIFFERENCE,
		SYMETRICDIFFERENCE,
		COMPLAMENT,
		CARTESIAN

	}
}

